Instructions for running the pacman project locally:
1. Pull the project from the repository ```git clone https://koisx@bitbucket.org/Wertragna/pacman.git```
2. Install node.js on your machine (https://nodejs.org/en/).
3. Open the terminal in the root of the project.
4. Run ```npm install``` to install project dependencies
5. Run ```npm start``` to start a local server
6. Follow the link from terminal to open the project in your browser:<br/>
![Server output](server-output.png?raw=true)
7. To stop the server press CTRL+C.
