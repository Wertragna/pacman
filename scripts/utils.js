export class Utils {
    static calculateExecutionTimeInMiliseconds(callback) {
        let args = [];
        for (let i = 1; i < arguments.length; i++) {
            args[i-1] = arguments[i];
        }
        let start = performance.now();
        let result = callback(...args);
        let end = performance.now();
        document.getElementById('time').innerHTML = String(end - start);
        return result;
    }

    static showNumberOfAlgorithmSteps(steps) {
        document.getElementById('steps').innerHTML = steps;
    }
}
