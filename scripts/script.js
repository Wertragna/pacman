import {Game} from "./game.js";
import {Pacman} from "./pacman.js";
import {Map} from "./map.js";

(function () {

    'use strict';

    let map = new Map(),
        pacman = new Pacman(map, 10, 10),
        game = new Game(map, pacman);

    window.onload = function () {
        setOnClickHandlerForStartButton();
        game.start();
        renderPacmanAnimation();
    };

    /**
     * Sets onClick handler for 'Start Game' button
     */
    function setOnClickHandlerForStartButton() {
        document.getElementById("generate").addEventListener("click", () => {
            game.start();
            let refreshId = setInterval(function () {
                game.render();
                if (!pacman.path || pacman.position === pacman.path.length - 1) {
                    clearInterval(refreshId);
                }
            }, 200);
        });
    }

    function renderPacmanAnimation() {
        let refreshId = setInterval(function () {
            game.render();
            if (!pacman.path || pacman.position === pacman.path.length - 1) {
                clearInterval(refreshId);
            }
        }, pacman.speed);
    }
})();
