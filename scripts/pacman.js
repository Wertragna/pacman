import {Utils} from './utils.js'
/**
 * This class represents the pacman's entity
 * and encapsulated its logic.
 */
export class Pacman {

    constructor(map, position_x, position_y) {
        this.x = position_x;
        this.y = position_y;
        this.width = map.tsize;
        this.height = map.tsize;
        this.position = 0;
        this.speed = 200;
    }

    generateWay(map, position, positionEnd) {

        this.matrix = [];
        for (let column = 0; column < map.tiles.length; column++) {
            this.matrix.push([...map.tiles[column]]);
        }
        this.position = 0;
        this.path = Utils.calculateExecutionTimeInMiliseconds(this.findWay.bind(this), position, positionEnd);
    }


    next() {
        this.position++;
        if (this.path) {
            const pathElement = this.path[this.position];
            if (pathElement) {
                this.x = pathElement[1];
                this.y = pathElement[0];
            }
        }
    }

    // BFS for pacman
    findWay(position, end) {
        let steps = 0;

        // this array will store the path to the food
        const queue = [];

        //mark the initial position on the map as visited
        this.matrix[position[0]][position[1]] = 1;
        // store the initial position as the beginning of the path
        queue.push([position]);

        while (queue.length > 0) {
            steps++;
            // get the first element of the path queue and remove it from it
            const path = queue.shift();
            //get the last position from the pacman's path
            const pos = path[path.length - 1];
            // stores 4 possible potential directions from the current position
            const direction = [
                [pos[0] + 1, pos[1]],
                [pos[0], pos[1] + 1],
                [pos[0] - 1, pos[1]],
                [pos[0], pos[1] - 1]
            ];

            for (let i = 0; i < direction.length; i++) {
                // if pacman reached the food, then the algorithm is done and we can return the resulting path
                if (direction[i][0] === end[0] && direction[i][1] === end[1]) {
                    Utils.showNumberOfAlgorithmSteps(steps);
                    // return path with the food position
                    return path.concat([end]);
                }

                // skip for those directions, which are either invalid or lead to the wall or are outside the map
                if (direction[i][0] < 0 || direction[i][0] >= this.matrix[0].length
                    || direction[i][1] < 0 || direction[i][1] >= this.matrix[0].length
                    || this.matrix[direction[i][0]][direction[i][1]] !== 0) {
                    continue;
                }

                // mark as visited
                this.matrix[direction[i][0]][direction[i][1]] = 1;
                // extend and push the path to the queue
                queue.push(path.concat([direction[i]]));
            }
        }
    }
}
