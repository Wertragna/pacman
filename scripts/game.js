/**
 * This class encapsulates pacman game logic
 */
export class Game {

    constructor(map, pacman) {
        this.pacman = pacman;
        this.map = map;
        this.init(map);
        this.render = this.render.bind(this);
        this.food = {};
    }

    init(map) {
        //canvas initialization
        const canvas = document.createElement('canvas');
        canvas.setAttribute('width', map.tsize * map.cols + 'px');
        canvas.setAttribute('height', map.tsize * map.rows + 'px');
        document.getElementById('pacman').appendChild(canvas);
        this.ctx = canvas.getContext('2d');
    }

    render() {
        for (let column = 0; column < this.map.tiles.length; column++) {
            for (let row = 0; row < this.map.tiles[column].length; row++) {

                switch (this.map.tiles[column][row]) {
                    case this.map.wallValue:
                        this.ctx.fillStyle = 'green';
                        break;
                    case this.map.roadValue:
                        this.ctx.fillStyle = 'lightgreen';
                        break;
                }
                this.ctx.fillRect(
                    row * this.map.tsize,
                    column * this.map.tsize,
                    this.map.tsize,
                    this.map.tsize
                );
            }
        }
        this.pacman.next();
        this.drawFood(this.food.x, this.food.y);
        this.drawPacman(this.pacman.x, this.pacman.y);

    }

    start() {
        this.generateNewFood();
        this.pacman.generateWay(this.map,
            [this.pacman.y, this.pacman.y],
            [this.food.y, this.food.x]
        );
    }


    generateNewFood() {
        const generateRandomIntFromInterval = (min, max) => {
            return Math.floor(Math.random() * (max - min + 1) + min);
        };

        /**
         * Generates the food, which is in a reachable position for pacman
         */
        const generateAcceptableFoodCoordinate = () => {
            let coordinateX, coordinateY;
            do {
                coordinateX = generateRandomIntFromInterval(0, 20);
                coordinateY = generateRandomIntFromInterval(0, 20);
            } while (this.map.tiles[coordinateY][coordinateX] !== this.map.roadValue);

            this.food.x = coordinateX;
            this.food.y = coordinateY;
        };

        generateAcceptableFoodCoordinate();
    }

    drawPacman(row, column) {
        this.renderObject('yellow', row, column);
    }

    drawFood(row, column) {
        this.renderObject('red', row, column);
    }

    renderObject(color, row, column) {
        this.ctx.fillStyle = color;
        this.ctx.beginPath();
        this.ctx.arc(
            row * this.map.tsize + this.map.tsize / 2,
            column * this.map.tsize + this.map.tsize / 2,
            this.map.tsize / 2,
            0,
            Math.PI * 2,
            true
        );
        this.ctx.fill();
    }
}
